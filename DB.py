import sqlite3
#erstellt datenbank, stellt verbindung zu dieser her
def erstellen():
    verbindung = sqlite3.connect("buttons.db")
    zeiger = verbindung.cursor()


    sql_anweisung_erstellenDB = """
    CREATE TABLE IF NOT EXISTS buttons (
    button VARCHAR,
    clickcount int
    );"""
    zeiger.execute(sql_anweisung_erstellenDB)

    sql_anweisung_Buttonsholen = """
    SELECT * FROM buttons
    """
    zeiger.execute(sql_anweisung_Buttonsholen)
    buttons = zeiger.fetchall()


    # INSERT IF NOT EXISTS
    if not buttons:
        zeiger.execute("""
                            INSERT INTO buttons 
                                   VALUES (?,?)
                           """,
                       ('1', '0')
                       )
        zeiger.execute("""
                            INSERT INTO buttons 
                                   VALUES (?,?)
                           """,
                       ('2', '0')
                       )
        zeiger.execute("""
                            INSERT INTO buttons 
                                   VALUES (?,?)
                           """,
                       ('3', '0')
                       )
        zeiger.execute("""
                            INSERT INTO buttons 
                                   VALUES (?,?)
                           """,
                       ('4', '0')
                       )
        zeiger.execute("""
                            INSERT INTO buttons 
                                   VALUES (?,?)
                           """,
                       ('5', '0')
                       )
        zeiger.execute("""
                            INSERT INTO buttons 
                                   VALUES (?,?)
                           """,
                       ('6', '0')
                       )
        zeiger.execute("""
                            INSERT INTO buttons 
                                   VALUES (?,?)
                           """,
                       ('7', '0')
                       )
        zeiger.execute("""
                            INSERT INTO buttons 
                                   VALUES (?,?)
                           """,
                       ('8', '0')
                       )
        zeiger.execute("""
                            INSERT INTO buttons 
                                   VALUES (?,?)
                           """,
                       ('9', '0')
                       )
        zeiger.execute("""
                            INSERT INTO buttons 
                                   VALUES (?,?)
                           """,
                       ('0', '0')
                       )
        zeiger.execute("""
                            INSERT INTO buttons 
                                   VALUES (?,?)
                           """,
                       ('+', '0')
                       )
        zeiger.execute("""
                            INSERT INTO buttons 
                                   VALUES (?,?)
                           """,
                       ('-', '0')
                       )
        zeiger.execute("""
                            INSERT INTO buttons 
                                   VALUES (?,?)
                           """,
                       ('*', '0')
                       )
        zeiger.execute("""
                            INSERT INTO buttons 
                                   VALUES (?,?)
                           """,
                       ('/', '0')
                       )
        zeiger.execute("""
                            INSERT INTO buttons 
                                   VALUES (?,?)
                           """,
                       ('(', '0')
                       )
        zeiger.execute("""
                            INSERT INTO buttons 
                                   VALUES (?,?)
                           """,
                       (')', '0')
                       )
        zeiger.execute("""
                            INSERT INTO buttons 
                                   VALUES (?,?)
                           """,
                       ('.', '0')
                       )
        zeiger.execute("""
                            INSERT INTO buttons 
                                   VALUES (?,?)
                           """,
                       ('=', '0')
                       )
        zeiger.execute("""
                            INSERT INTO buttons 
                                   VALUES (?,?)
                           """,
                       ('B', '0')
                       )
        zeiger.execute("""
                            INSERT INTO buttons 
                                   VALUES (?,?)
                           """,
                       ('C', '0')
                       )
    verbindung.commit()
    verbindung.close()


#Funktion zum Updaten der Knopfdruecke
def update(char):
    verbindung = sqlite3.connect("buttons.db")
    zeiger = verbindung.cursor()

    zeiger.execute("SELECT clickcount FROM buttons WHERE button=?", char)
    clickcount = zeiger.fetchone()
    print(clickcount[0])
    count = clickcount[0] +1
    zeiger.execute("UPDATE buttons SET clickcount=? WHERE button=?", (count, char))

    verbindung.commit()
    verbindung.close()




