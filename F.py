from flask import Flask, url_for, request, render_template

import DB

app = Flask(__name__)


@app.route("/")     #Pfad der zur seite führt. Die funktion wird ausgeführt
def index():
    return render_template("TRechner.html")   #Gibt die html datei zurück/zeigt sie an

@app.route("/DB")
def save():
    value = request.args.get("char") #holt die daten, welcher button gedrückt wurde(get anfrage schreibt die daten in url)
    if value == "p":
        value= "+"
    DB.update(value)   #ruft die funktion auf in der das update der gedrückten taste macht
    return""

if __name__=="__main__":
    DB.erstellen()
    app.run(port=1337, debug=True)    #startet den server
    app.add_url_rule('/favicon-16x16.png',
                     redirect_to=url_for('static', filename='favicon-16x16.png'))